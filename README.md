# MVI - Semestrální práce - TensorFlow Speech Recognition Challenge

Cílem je klasifikovat slova, která jsou řečena na jednotlivých krátkých nahrávkách. Více informac viz. [Kaggle](https://www.kaggle.com/c/tensorflow-speech-recognition-challenge/overview).

### Data a spuštění projektu

Pro získání dat je nutné být registrován na Kaggle a stáhnout si dataset ze soutěže z linku výše.

Projekt je psán v Jupyter notebooku, doporučuji si tedy nahrát tento repozitář do Google Colabu a 
do složky data nahrát komprimovaný soubor s trénovacími daty. Poté už stačí pouze spustit daný notebook.

### Článek na Medium

Článek viz. [link](https://medium.com/@curdopet/spoken-word-classification-tensorflow-speech-recognition-challenge-83bf9210bda9).